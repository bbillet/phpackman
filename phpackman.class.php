<?php
/**
 * PHPackman Libary Class Wrapper
 * https://bitbucket.org/bbillet/phpackman
 * 
 * A simple library for generating auto-extractible autonomous archives in PHP.
 * The generated archives are PHP files that contains multiple files (compressed or juste base64-encoded) and
 * the code necessary to rebuild the hierarchy of files. 
 * The archive is perfectly autonomous, the PHPackman library is only required to build the archive.
 *
 * Note: Compression uses the ZipArchive class (http://php.net/ziparchive, PHP 5 >= 5.2.0).
 *
 * @author Benjamin Billet <contact@benjaminbillet.fr>
 * @version 0.1
 */
 
include 'phpackman.lib.php';

class PHPackman
{
	private $phpackman;

	/**
	 * Build a PHPackman instance.
	 * @param string $outputFolder the folder where the archive will be extracted when executed (default = the current folder).
	 * @param boolean $compress enable or disable the compression.
	 * @param string $redirectAfter the redirection URI that will be used after the archive extraction.
	 * @param string $chmod the access restrictions of the files after their extraction.
	 */
	public function __construct($outputFolder = '.', $compress = true, $redirectAfter = null, $chmod = '0605')
	{
		$this->phpackman = phpackman_create($outputFolder, $compress, $redirectAfter, $chmod);
	}
   
	/**
	 * Add a file into the archive.
	 * @param string $filePath the path of the file that will be added to the archive.
	 * @param string $localFilePath the local path of the file in the archive (default = $filePath).
	 */
	public function addFile($filePath, $localFilePath = null)
	{
		phpackman_add_file($this->phpackman, $filePath, $localFilePath);
	}
	
	/**
	 * Add a folder recursively into a PHPackman archive.
	 * @param string $folderPath the path of the folder that will be added to the archive.
	 * @param string $localFolderPath the local path of the folder in the archive (default = $folderPath).
	 */
	public function addFolder($folderPath, $localFolderPath = null)
	{
		phpackman_add_folder($this->phpackman, $folderPath, $localFolderPath);
	}
	
	/**
	 * Build the archive.
	 * @param string $outputFile the path of the file where the data will be written.
	 * @param integer $chunkSize the size of the chunks in bytes (when files are added into the archive, they are divided into chunks 
	 * 					in order to avoid loading all files in the memory while building).
	 */
	public function build($outputFile, $chunkSize = 1048576)
	{
		phpackman_build($this->phpackman, $outputFile, $chunkSize);
	}
}
?>
