<?php
/**
 * PHPackman Libary
 * https://bitbucket.org/bbillet/phpackman
 * 
 * A simple library for generating auto-extractible autonomous archives in PHP.
 * The generated archives are PHP files that contains multiple files (compressed or juste base64-encoded) and
 * the code necessary to rebuild the hierarchy of files. 
 * The archive is perfectly autonomous, the PHPackman library is only required to build the archive.
 *
 * Note: Compression uses the ZipArchive class (http://php.net/ziparchive, PHP 5 >= 5.2.0).
 *
 * @author Benjamin Billet <contact@benjaminbillet.fr>
 * @version 0.1
 */
 
/**
 * Configure a PHPackman instance.
 * @param string $output_folder the folder where the archive will be extracted when executed (default = the current folder).
 * @param boolean $compress enable or disable the compression.
 * @param string $redirect_after the redirection URI that will be used after the archive extraction.
 * @param string $chmod the access restrictions of the files after their extraction.
 * @return the instance (must be given to other functions). 
 */
function phpackman_create($output_folder = '.', $compress = true, $redirect_after = null, $chmod = '0605')
{
	return array('compress' => $compress, 'redirect' => $redirect_after, 'chmod' => $chmod, 'output_folder' => $output_folder, 'files' => array());
}

/**
 * Add a file into a PHPackman instance.
 * @param phpackman $phpackman the PHPackman instance.
 * @param string $file_path the path of the file that will be added to the instance.
 * @param string $local_file_path the local path of the file in the archive (default = $file_path).
 */
function phpackman_add_file(&$phpackman, $file_path, $local_file_path = null)
{
	if($local_file_path == null)
		$phpackman['files'][$file_path] = $file_path;
	else
		$phpackman['files'][$local_file_path] = $file_path;
}

/**
 * Add a folder recursively into a PHPackman instance.
 * @param phpackman $phpackman the PHPackman instance.
 * @param string $folder_path the path of the folder that will be added to the instance.
 * @param string $local_folder_path the local path of the folder in the archive (default = $folder_path).
 */
function phpackman_add_folder(&$phpackman, $folder_path, $local_folder_path = null)
{
	$dir = opendir($folder_path) or die('Cannot open the folder');		
	while($file = @readdir($dir)) 
	{
		$real_path = $folder_path . '/' . $file;
		$new_path = $local_folder_path . '/' . $file;
		if(is_dir($real_path) && $file != '.' && $file != '..')
		{
			if($local_folder_path == null)
				phpackman_add_folder($phpackman, $real_path, null);
			else
				phpackman_add_folder($phpackman, $real_path, $new_path);
		}
		else if(is_file($real_path))
		{
			if($local_folder_path == null)
				phpackman_add_file($phpackman, $real_path, null);
			else
				phpackman_add_file($phpackman, $real_path, $new_path);
		}
	}
	closedir($dir);
}

/**
 * Build the archive.
 * @param phpackman $phpackman the PHPackman instance.
 * @param string $output_file the path of the file where the data will be written.
 * @param integer $chunk_size the size of the chunks in bytes (when files are added into the archive, they are divided into chunks 
 * 					in order to avoid loading all files in the memory while building).
 */
function phpackman_build($phpackman, $output_file, $chunk_size = 1048576)
{
	$output = fopen($output_file, 'wb') or die('Cannot open the output file');
	phpackman_write_header($output, $phpackman['output_folder'], $phpackman['compress'], $phpackman['redirect'], $phpackman['chmod']);
		
	if($phpackman['compress'])
		phpackman_build_compressed($phpackman, $output, $chunk_size);
	else
		phpackman_build_uncompressed($phpackman, $output, $chunk_size);
		
	fwrite($output, "\n\n" . 'phpackman_autoextract($phpackman_data);' . "\n" . '?>');
	fclose($output);
}

/** 
 * Build archive content without compression.
 * Private, you must not use this function directly. See phpackman_build.
 * @param phpackman $phpackman the PHPackman instance.
 * @param string $output the output file handler.
 * @param integer $chunk_size the size of the chunks.
 */
function phpackman_build_uncompressed($phpackman, $output, $chunk_size)
{
	fwrite($output, '$phpackman_data = array(' . "\n");

	$files = $phpackman['files'];
	foreach($files as $file => $real_file)
	{
		fwrite($output, '	\'' . $file . '\' => array(');
		phpackman_chunkify($real_file, $output, $chunk_size);
		fwrite($output, '),' . "\n");
	}
	
	fwrite($output, ');');
}

/** 
 * Build archive content with compression.
 * Private, you must not use this function directly. See phpackman_build.
 * @param phpackman $phpackman the PHPackman instance.
 * @param string $output the output file handler.
 * @param integer $chunk_size the size of the chunks.
 */
function phpackman_build_compressed($phpackman, $output, $chunk_size)
{
	$archive = 'phpackman_tmp' . uniqid() . '.zip';
	$zip = new ZipArchive();
	$zip->open($archive, ZipArchive::CREATE) or die('Cannot create the archive');

	$files = $phpackman['files'];
	foreach($files as $file => $real_file)
	{
		$zip->addFile($real_file, $file);
	}
	$zip->close();
	
	fwrite($output, '$phpackman_data = array(');
	phpackman_chunkify($archive, $output, $chunk_size);
	fwrite($output, ');');
		
	unlink($archive);
}

/** 
 * Write the headers in the archive file.
 * Private, you must not use this function.
 * @param string $output the output file handler.
 * @param string $output_folder the path to the output folder where file will be extracted.
 * @param boolean $compress enable or disable the compression.
 * @param string $redirect_after the redirection URI that will be used after the archive extraction.
 * @param string $chmod the access restrictions of the files after their extraction.
 */
function phpackman_write_header($output, $output_folder, $compress, $redirect_after, $chmod)
{
	$data = '<?php' . "\n";
	$data .= '// Packed with PHPackman' . "\n";
	$data .= '// https://bitbucket.org/bbillet/phpackman' . "\n\n";
	
	$data .= 'function phpackman_recompose($output, array $chunks)' . "\n";
	$data .= '{' . "\n";
	$data .= '	foreach($chunks as $chunk)' . "\n";
	$data .= '	{' . "\n";
	$data .= '		if(fwrite($output, base64_decode($chunk)) === false)' . "\n";
	$data .= '			return false;' . "\n";
	$data .= '	}' . "\n";
	$data .= '	return true;' . "\n";
	$data .= '}' . "\n\n";
	
	$data .= 'function phpackman_autoextract($data)' . "\n{\n";
	$data .= '	if(file_exists(\'' . $output_folder . '\') === false)' . "\n";
	$data .= '		mkdir(\'' . $output_folder . '\', ' . $chmod . ', true);' . "\n\n";

	if($compress)
	{
		$data .= '	$archive = \'phpackman_tmp\' . uniqid() . \'.zip\';' . "\n";
		$data .= '	$output = fopen($archive, \'wb\') or die(\'Cannot write the temporary files !\');' . "\n";
		$data .= '	phpackman_recompose($output, $data) or die(\'Cannot write the temporary files !\');' . "\n";
		$data .= '	fclose($output);' . "\n\n";
		
		$data .= '	$zip = new ZipArchive();' . "\n";
		$data .= '	$zip->open($archive) or die(\'Cannot open the temporary files !\');' . "\n";
		$data .= '	$zip->extractTo(\'' . $output_folder . '\') or die(\'Cannot extract the files !\');' . "\n";
		$data .= '	$zip->close();' . "\n";
		$data .= '	unlink($archive);' . "\n";
	}
	else
	{
		$data .= '	foreach($data as $path => $chunks)' . "\n";
		$data .= '	{' . "\n";
		$data .= '		$path = \'' . $output_folder . '/\' . $path;' . "\n";
		$data .= '		if(file_exists(dirname($path)) === false)' . "\n";
		$data .= '			mkdir(dirname($path), ' . $chmod . ', true);' . "\n\n";
			
		$data .= '		$output = fopen($path, \'wb\') or die(\'Cannot write the files !\');' . "\n";
		$data .= '		phpackman_recompose($output, $chunks) or die(\'Cannot write the files !\');' . "\n";
		$data .= '		fclose($output);' . "\n";
		$data .= '	}' . "\n";
	}
	
	if($redirect_after != null)
		$data .= '	header(\'Location: ' . $redirect_after . '\');' . "\n";
	
	$data .= "}\n\n";
	fwrite($output, $data) or die('Cannot write in the archive');
}

/** 
 * Split a file in chunks and write it into the archive.
 * Private, you must not use this function.
 * @param string $file_source the file to split.
 * @param string $output the output file handler.
 * @param integer $chunk_size the size of the chunks.
 */
function phpackman_chunkify($file_source, $output, $chunk_size)
{
	$handle = fopen($file_source, 'rb') or die('Cannot open the file ' . $file_source);		
	while(feof($handle) === false)
	{
		$chunk = fread($handle, $chunk_size) or die('Cannot read the file ' . $file_source);
		fwrite($output, '\'' . base64_encode($chunk) . '\',') or die('Cannot write in the archive');
	}
	
	fclose($handle);
}
?>
