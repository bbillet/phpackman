<?php
	include '../phpackman.class.php';
	
	$phpackman = new PHPackman('output_folder1', true, null, '0777');
	$phpackman->addFile('file_test1', 'file_test1');
	$phpackman->addFile('file_test2', 'a_test_folder/file_test2');
	$phpackman->addFile('folder_test/file_test1', 'folder_test/file_test1');
	$phpackman->addFile('folder_test/file_test2', 'file_test2');
	$phpackman->addFile('file_test2', 'folder_test/file_test2');
	$phpackman->addFile('folder_test/file_test1', 'a_test_folder/file_test1');
	$phpackman->addFolder('folder_test', 'another_test_folder');
	
	$phpackman->build('my_packed_file_compressed.pack.php');
	
	$phpackman = new PHPackman('output_folder2', false, null, '0777');
	$phpackman->addFile('file_test1', 'file_test1');
	$phpackman->addFile('file_test2', 'a_test_folder/file_test2');
	$phpackman->addFile('folder_test/file_test1', 'folder_test/file_test1');
	$phpackman->addFile('folder_test/file_test2', 'file_test2');
	$phpackman->addFile('file_test2', 'folder_test/file_test2');
	$phpackman->addFile('folder_test/file_test1', 'a_test_folder/file_test1');
	$phpackman->addFolder('folder_test', 'another_test_folder');
	
	$phpackman->build('my_packed_file_uncompressed.pack.php');
?>
