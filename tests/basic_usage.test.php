<?php
	include '../phpackman.lib.php';
	
	$phpackman = phpackman_create('output_folder1', true, null, '0777');
	phpackman_add_file($phpackman, 'file_test1', 'file_test1');
	phpackman_add_file($phpackman, 'file_test2', 'a_test_folder/file_test2');
	phpackman_add_file($phpackman, 'folder_test/file_test1', 'folder_test/file_test1');
	phpackman_add_file($phpackman, 'folder_test/file_test2', 'file_test2');
	phpackman_add_file($phpackman, 'file_test2', 'folder_test/file_test2');
	phpackman_add_file($phpackman, 'folder_test/file_test1', 'a_test_folder/file_test1');
	phpackman_add_folder($phpackman, 'folder_test', 'another_test_folder');
	
	phpackman_build($phpackman, 'my_packed_file_compressed.pack.php');
	
	$phpackman = phpackman_create('output_folder2', false, null, '0777');
	phpackman_add_file($phpackman, 'file_test1', 'file_test1');
	phpackman_add_file($phpackman, 'file_test2', 'a_test_folder/file_test2');
	phpackman_add_file($phpackman, 'folder_test/file_test1', 'folder_test/file_test1');
	phpackman_add_file($phpackman, 'folder_test/file_test2', 'file_test2');
	phpackman_add_file($phpackman, 'file_test2', 'folder_test/file_test2');
	phpackman_add_file($phpackman, 'folder_test/file_test1', 'a_test_folder/file_test1');
	phpackman_add_folder($phpackman, 'folder_test', 'another_test_folder');
	
	phpackman_build($phpackman, 'my_packed_file_uncompressed.pack.php');
?>
